import { Component, OnInit } from '@angular/core';
import sampleData from 'src/assets/sample_data.json';
import { TableService } from './table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  private RowsInput: HTMLInputElement;
  private PageLength = 10;
  private MaxPages: number;
  private CurrentPage = 1;

  public fieldsToHeaders = {
    name: 'Name',
    phone: 'Phone',
    email: 'Email',
    company: 'Company',
    date_entry: 'Date Entry',
    org_num: 'Org Number',
    address_1: 'Street Address',
    city: 'City',
    zip: 'Zipcode',
    geo: 'Geo Coords',
    pan: 'Pan',
    pin: 'Pin',
    id: 'ID',
    status: 'Status',
    fee: 'Fee',
    guid: 'GUID',
    date_exit: 'Date Exit',
    date_first: 'Date First',
    date_recent: 'Date Recent',
    url: 'URL'
  };

  get data(): {}[] {
    return sampleData;
  }

  set currentPage(newPage: number) {
    if (newPage <= this.MaxPages && newPage >= 1) {
      this.CurrentPage = newPage;
    }
  }

  get currentPage(): number {
    return this.CurrentPage;
  }

  get pageLength(): number {
    return this.PageLength;
  }

  get maxPages(): number {
    return this.MaxPages;
  }

  constructor(public tableService: TableService) { }

  ngOnInit() {
    this.RowsInput = document.getElementById('rowsInput') as HTMLInputElement;
    this.MaxPages = this.data.length / this.pageLength;
  }

  public setPageLength() {
    this.PageLength = Number.parseInt(this.RowsInput.value, 10);
    this.MaxPages = this.data.length / this.pageLength;
  }

}
