import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor(private Http: HttpClient) { }

  /** POST: submit the id and status of a row */
  public post(item: {id: number, status: string}) {
    const postContent = {id: item.id, status: item.status};
    console.log(postContent);
    this.Http.post('/api/submit', postContent);
  }

}
